package singletonEx2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LoadingConfigFile {
    public int getZakresLiczbyPierwszej() {
        return zakresLiczbyPierwszej;
    }

    public void setZakresLiczbyPierwszej(int zakresLiczbyPierwszej) {
        this.zakresLiczbyPierwszej = zakresLiczbyPierwszej;
    }

    public int getZakresLiczbyDrugiej() {
        return zakresLiczbyDrugiej;
    }

    public void setZakresLiczbyDrugiej(int zakresLiczbyDrugiej) {
        this.zakresLiczbyDrugiej = zakresLiczbyDrugiej;
    }

    public List<String> getDostępneDziałania() {
        return dostępneDziałania;
    }

    public void setDostępneDziałania(List<String> dostępneDziałania) {
        this.dostępneDziałania = dostępneDziałania;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }

    private int zakresLiczbyPierwszej;

    private int zakresLiczbyDrugiej;

    private List<String> dostępneDziałania = new ArrayList<>();

    private int iloscRund;



    public void loadFromFile(){

        try (Scanner scanner = new Scanner(new File("configFile.txt"))) {

            this.zakresLiczbyPierwszej = Integer.valueOf(scanner.nextLine());
            this.zakresLiczbyDrugiej = Integer.valueOf(scanner.nextLine());

            String buffor = scanner.nextLine();
            String[] buffor2 = buffor.split(",");

            for(int i = 0;i<buffor2.length;i++) {

                this.dostępneDziałania.add(buffor2[i]);
            }

            this.iloscRund = Integer.valueOf(scanner.nextLine());

            MySetting.INSTANCE.setIloscRund(iloscRund);
            MySetting.INSTANCE.setDostępneDziałania(dostępneDziałania);
            MySetting.INSTANCE.setZakresLiczbyDrugiej(zakresLiczbyDrugiej);
            MySetting.INSTANCE.setZakresLiczbyPierwszej(zakresLiczbyPierwszej);

        } catch (FileNotFoundException e) {
            System.out.println("No file downloaded");

            //e.printStackTrace();
        }







    }


}
