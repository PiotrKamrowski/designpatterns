package singletonEx2;

import java.util.ArrayList;
import java.util.List;

public enum  MySetting {

INSTANCE;


    private int zakresLiczbyPierwszej;

    private int zakresLiczbyDrugiej;

    private List<String> dostępneDziałania = new ArrayList<>();

    private int iloscRund;



























    public int getZakresLiczbyPierwszej() {
        return zakresLiczbyPierwszej;
    }

    public void setZakresLiczbyPierwszej(int zakresLiczbyPierwszej) {
        this.zakresLiczbyPierwszej = zakresLiczbyPierwszej;
    }

    public int getZakresLiczbyDrugiej() {
        return zakresLiczbyDrugiej;
    }

    public void setZakresLiczbyDrugiej(int zakresLiczbyDrugiej) {
        this.zakresLiczbyDrugiej = zakresLiczbyDrugiej;
    }

    public List<String> getDostępneDziałania() {
        return dostępneDziałania;
    }

    public void setDostępneDziałania(List<String> dostępneDziałania) {
        this.dostępneDziałania = dostępneDziałania;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }
}
